//
//  AppleSwiftUITutorialApp.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 05/04/22.
//

import SwiftUI

@main
struct AppleSwiftUITutorialApp: App {
    let persistence = CoreDataDB.shared
    var body: some Scene {
        WindowGroup {
            PersonList()
                .environment(\.managedObjectContext, persistence.viewContext)
        }
    }
}

