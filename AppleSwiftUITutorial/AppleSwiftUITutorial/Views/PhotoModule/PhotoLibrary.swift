//
//  PhotoLibrary.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 12/04/22.
//

import Foundation
import SwiftUI

public struct PhotoLibrary: UIViewControllerRepresentable {
    
    public func makeUIViewController(context: Context) -> some UIViewController {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = context.coordinator
        return picker
    }
    
    public func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    //Coordinator must be NSObject
    public func makeCoordinator() -> PhotoCoordinator {
        PhotoCoordinator()
    }
}

public class PhotoCoordinator: NSObject,
                               UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
    @Published var selectedImages = [UIImage]()

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        selectedImages.append(img)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

