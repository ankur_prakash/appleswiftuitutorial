//
//  CollageView.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 12/04/22.
//

import SwiftUI

struct CollageView: View {
    @StateObject var model = ViewModel()

    var body: some View {
        VStack(spacing: 10) {
            TopHeaderView(images: $model.images, model: model)
            CollageGridView()
                .frame(height: 200.0)
                .frame(maxWidth:.infinity)
                .border(Color.black, width: 2.0)
            Button(action: model.clear) {
                 Text("Clear")
                    .foregroundColor(.white)
                    .frame(height: 44.0, alignment: .center)
                    .frame(maxWidth: .infinity)
                    .background(Color.red)
                    .cornerRadius(10.0)
                    .clipped()
            }
            Button(action: model.save) {
                 Text("Save")
                    .foregroundColor(.white)
                    .frame(height: 44.0, alignment: .center)
                    .frame(maxWidth: .infinity)
                    .background(Color.green)
                    .cornerRadius(10.0)
                    .clipped()
            }
        }
        .padding()
        .sheet(isPresented: $model.isVisible, onDismiss: {
            model.isVisible = false
        }, content: {
            PhotoLibrary()
        })

    }
}

extension CollageView {
    
    struct TopHeaderView: View {
        
        @Binding var images: [UIImage]
        @ObservedObject var model: ViewModel
        
        var body: some View {
            HStack {
                if images.count != 0 {
                    Text("\(images.count) photos")
                        .font(.title)
                        .bold()
                }
                Spacer()
                Button {
                    model.showLibrary()
                } label: {
                    Label<Text, Image>(title: { Text("") }, icon: { Image(systemName: "plus") })
                }.font(.title)
            }
        }
    }

    
    class ViewModel: ObservableObject {
       
        @Published var images: [UIImage] = []
        @Published var isVisible = false

        func showLibrary() {
            isVisible = true
        }
        
        func clear() {
            images.removeAll()
        }
        
        func save() {
            print("save pressed")
            images.append(UIImage())
        }
    }
}
struct CollageView_Previews: PreviewProvider {
    static var previews: some View {
        CollageView()
    }
}
