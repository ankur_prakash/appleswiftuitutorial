//
//  BindingDemo.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 12/04/22.
//

import SwiftUI

// Binding is connected with State
// we can bind state e.g @State and @StateObject

struct BindingDemo: View {
    @State var color = Color.red
    @State var text = "Demo"
    
    var body: some View {
        VStack {
            LabelView(text: $text, color: $color)
            Button("Press Me") {
                color = .blue
                text = "Demo 1"
            }

        }
    }
}

struct BindingDemo_Previews: PreviewProvider {
    static var previews: some View {
        BindingDemo()
    }
}

public struct LabelView: View {

    @Binding var text: String
    @Binding var color: Color
    
    public var body: some View {
        Text(text)
            .background(color)
    }
}
