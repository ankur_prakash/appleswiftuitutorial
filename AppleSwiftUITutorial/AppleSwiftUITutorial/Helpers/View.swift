//
//  View.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 08/04/22.
//

import Foundation
import UIKit
import SwiftUI

public extension View {
    
    func barColor(_ backgroundColor: UIColor) -> some View {
        self.modifier(ColoredNavigationBar(backgroundColor: backgroundColor))
    }
}
