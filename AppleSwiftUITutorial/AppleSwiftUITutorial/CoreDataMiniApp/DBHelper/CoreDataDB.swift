//
//  DataManager.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 15/04/22.
//

import Foundation
import Combine

class CoreDataDB {
    
    private static let databaseFileName = "Tutorial"
    static let shared = CoreDataDB(name: databaseFileName, needsCloudKit: true)
    let coreDataStack: CoreDataStack
    lazy var viewContext = self.coreDataStack.container.viewContext
    
    private init(name: String, needsCloudKit: Bool) {
        self.coreDataStack = CoreDataStack(name: name, needsCloudKit: needsCloudKit)
    }
}

extension CoreDataDB: EmployeeDB {
    func allEmployees() -> AnyPublisher<[Employee], Error> {
        Empty().eraseToAnyPublisher()
    }
    
    func updateEmployee(_ employee: Employee) -> AnyPublisher<Void, Error> {
        return Empty().eraseToAnyPublisher()
    }
    
    func deleteEmployee(_ employee: Employee) -> AnyPublisher<Void, Error> {
        return Empty().eraseToAnyPublisher()
    }
    
    
    
}
