//
//  PersonList.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 15/04/22.
//

import SwiftUI
import CoreData

struct PersonList: View {
    
    //Directly access environment
    @Environment(\.managedObjectContext) private var viewContext
    
    @FetchRequest(sortDescriptors: [NSSortDescriptor.init(keyPath: \EmployeeMO.name, ascending: true)], predicate: nil, animation: .default)
    var employees: FetchedResults<EmployeeMO>
    
    var body: some View {
        List(employees) { item in
            Text(item.name ?? "")
        }
    }
}

struct PersonList_Previews: PreviewProvider {
    static var previews: some View {
        PersonList()
    }
}
