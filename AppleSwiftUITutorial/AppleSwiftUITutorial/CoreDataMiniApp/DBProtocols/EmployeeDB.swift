//
//  PersonDB.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 15/04/22.
//

import Foundation
import Combine

public protocol EmployeeDB {
    func allEmployees() -> AnyPublisher<[Employee], Error>
    func updateEmployee(_ employee: Employee) -> AnyPublisher<Void, Error>
    func deleteEmployee(_ employee: Employee) -> AnyPublisher<Void, Error>
}
