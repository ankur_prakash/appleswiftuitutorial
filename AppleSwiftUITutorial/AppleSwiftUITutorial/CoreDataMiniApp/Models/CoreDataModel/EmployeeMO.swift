//
//  EmployeeMO.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 15/04/22.
//

import Foundation
import CoreData

@objc
public class EmployeeMO: NSManagedObject {
 
    @NSManaged var uuid: UUID
    @NSManaged var name: String
    @NSManaged var designation: String
    @NSManaged var department: String
    @NSManaged var dob: Date?
    @NSManaged var joiningDate: Date
    @NSManaged var exitDate: Date?
    @NSManaged var manager: Manager?
}

extension EmployeeMO: Identifiable {
    public var id: String {
        uuid.uuidString
    }
}
