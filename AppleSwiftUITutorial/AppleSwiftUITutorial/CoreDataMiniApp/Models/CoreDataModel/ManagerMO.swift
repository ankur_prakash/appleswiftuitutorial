//
//  ManagerMO.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 15/04/22.
//

import Foundation
import CoreData

class ManagerMO: NSManagedObject {
    
    @NSManaged var seldDetail: Employee
    @NSManaged var team: [Employee]
}
