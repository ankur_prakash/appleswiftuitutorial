//
//  Persistence.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 15/04/22.
//

import Foundation
import CoreData

class CoreDataStack {
    
    //storageType: StorageType
    let container: NSPersistentContainer
    public init(name: String, needsCloudKit: Bool) {
        self.container = needsCloudKit ? NSPersistentCloudKitContainer(name: name): NSPersistentContainer(name: name)
        //What is parent Context
        self.container.viewContext.automaticallyMergesChangesFromParent = true
        self.container.loadPersistentStores { storeDescription, error in
            guard let err = error else {
                print("PersistenceStore loaded Successfully")
                return
            }
            fatalError("PersistenceStore failed to load = \(err.localizedDescription)")
        }
    }
    
}
