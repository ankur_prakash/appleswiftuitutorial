//
//  ContentView.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 05/04/22.
//

import SwiftUI

struct RootView: View {
    
    enum TabOption {
        case usecase
        case camera
        case history
    }
    
    @State private var selection: TabOption = .usecase
    
    var body: some View {
        TabView(selection: $selection) {
             UseCasesView()
                .tabItem {
                    Label("UseCases", systemImage: "star")
                }
                .tag(TabOption.usecase)
            
            CameraView()
               .tabItem {
                   Label("Camera", systemImage: "camera")
               }
               .tag(TabOption.usecase)
            
            HistoryView()
               .tabItem {
                   Label("History", systemImage: "clock")
               }
               .tag(TabOption.usecase)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
