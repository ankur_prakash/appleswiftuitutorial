//
//  FrameManager.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 09/04/22.
//

import Foundation
import AVFoundation

class FrameManager: NSObject, ObservableObject {
    
    static let shared = FrameManager()
    //Add a published property for the current frame received from the camera. This is what other classes will subscribe to to get the camera data.
    @Published var current: CVPixelBuffer?
   
    let videoOutputQueue = DispatchQueue(
        label: "com.mavenir.VideoOutputQueue",
         qos: .userInitiated,
        attributes: [],
        autoreleaseFrequency: .workItem)
                                                 
    override private init() {
        super.init()
        CameraManager.shared.set(self, queue: videoOutputQueue)
    }
}

extension FrameManager: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        if let buffer = sampleBuffer.imageBuffer {
            //Once again, since current is a published property, it needs to be set on the main thread. That’s that. Short and simple.
            DispatchQueue.main.async {
                self.current = buffer
            }
        }
    }
}
