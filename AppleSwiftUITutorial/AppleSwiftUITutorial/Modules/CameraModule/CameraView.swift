//
//  CameraView.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 08/04/22.
//

import SwiftUI

struct CameraView: View {
    
    @StateObject private var model = CameraViewModel()
    
    var body: some View {
        FrameView(image: model.frame)
            .edgesIgnoringSafeArea(.all)
    }
}

struct CameraView_Previews: PreviewProvider {
    static var previews: some View {
        CameraView()
    }
}

extension CameraView {
    
    class CameraViewModel: ObservableObject {
        
        @Published var frame: CGImage?
        private let frameManager = FrameManager.shared
        
        init() {
            setupSubscriptions()
          }
        
        func setupSubscriptions() {
            
            frameManager.$current
                .receive(on: RunLoop.main)
                .compactMap { buffer in
                    CGImage.create(from: buffer)
                }.assign(to: &$frame)
          }
    }
}
