//
//  CameraManager.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 08/04/22.
//

import Foundation
import SwiftUI
import AVFoundation

public enum CameraError: Error {
    case deniedAuthorization
    case restrictedAuthorization
    case unknownAuthorization
    case cameraUnavailable
    case cannotAddInput
    case createCaptureInput(Error)
    case cannotAddOutput
}

class CameraManager: ObservableObject {
    
    @Published var error: CameraError?
    //AVCaptureSession, which will coordinate sending the camera images to the appropriate data output.
    let session = AVCaptureSession()
    //A session queue, which you’ll use to change any of the camera configurations.
    private let sessionQueue = DispatchQueue(label: "com.mavenir.SessionQ")
    //AVCaptureVideoDataOutput to connect to AVCaptureSession. You’ll want this stored as a property so you can change the delegate after the session is configured.
    private let videoOutput = AVCaptureVideoDataOutput()
    private var status: Status = .unconfigured
    
    
    enum Status {
        case unconfigured
        case configured
        case unauthorized
        case failed
    }
    
    static let shared = CameraManager()
    
    private init() {
        configure()
    }
    
    private func configure() {
        checkPermissions()
        sessionQueue.async {
            self.configureCaptureSession()
            self.session.startRunning()
        }
    }
    
    private func set(error: CameraError?) {
        DispatchQueue.main.async {
            self.error = error // Why any published property should be on main thread
        }
    }
    
    private func checkPermissions() {
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .notDetermined:
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: .video) { authorzied in
                if !authorzied {
                    self.status = .unauthorized
                    self.set(error: .deniedAuthorization)
                }
                self.sessionQueue.resume()
            }
        case .restricted:
            status = .unauthorized
            set(error: .restrictedAuthorization)
        case .denied:
            status = .unauthorized
            set(error: .deniedAuthorization)
        case .authorized:
            break
        @unknown default:
            status = .unauthorized
            set(error: .unknownAuthorization)
        }
    }
    
    private func configureCaptureSession() {
        guard status == .unconfigured else {
            print("One time session already configured")
            return }
        
        session.beginConfiguration()
        
        defer {
            session.commitConfiguration()
        }
        
        guard let device = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                   for: .video,
                                                   position: .front) else {
            set(error: .cameraUnavailable)
            status = .failed
            return
        }
        
        do {
            let cameraInput = try AVCaptureDeviceInput(device: device)
            if session.canAddInput(cameraInput) {
                session.addInput(cameraInput)
            } else {
                set(error: .cannotAddInput)
                status = .failed
                return
            }
        } catch {
            set(error: .createCaptureInput(error))
            status = .failed
            return
        }
        
        if session.canAddOutput(videoOutput) {
            session.addOutput(videoOutput)
            videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
            let videoConnection = videoOutput.connection(with: .video)
            videoConnection?.videoOrientation = .portrait
        } else {
            set(error: .cannotAddOutput)
            status = .failed
            return
        }
        status = .configured
    }
    
    func set( _ delegate: AVCaptureVideoDataOutputSampleBufferDelegate,
              queue: DispatchQueue ) {
        
        sessionQueue.async {
            self.videoOutput.setSampleBufferDelegate(delegate, queue: queue)
        }
    }
}

