//
//  AVCaptureSessionView.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 08/04/22.
//

import Foundation
import SwiftUI

struct AVCaptureSessionView: UIViewControllerRepresentable {
    
    func makeUIViewController(context: Context) -> some UIViewController {
        UIViewController()
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    func makeCoordinator() -> AVCaptureSessionCoordinator {
        return AVCaptureSessionCoordinator()
    }
}

