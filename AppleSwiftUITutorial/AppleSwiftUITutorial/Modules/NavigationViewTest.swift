//
//  NavigationViewTest.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 05/04/22.
//

import SwiftUI

struct NavigationViewTest: View {
    
    var body: some View {
        NavigationView {
            VStack {
                Spacer()
                Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
                    .navigationTitle(.init("TEst"))
                    .navigationBarTitleDisplayMode(.inline)
                .navigationViewStyle(DefaultNavigationViewStyle())
                Spacer()
            }
            .background(Color.red)
        }
    }
}

struct NavigationViewTest_Previews: PreviewProvider {
    static var previews: some View {
        NavigationViewTest()
    }
}
