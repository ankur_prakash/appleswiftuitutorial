//
//  UseCasesRow.swift
//  IVA
//
//  Created by Ankur Prakash on 04/04/22.
//

import SwiftUI

struct UseCasesRow: View {
 
    var title: String
    var body: some View {
        VStack(alignment: .leading, spacing: 12.0) {
            Group {
                Text(title)
                    .bold()
                HStack {
                    Button(action: {
                        
                    }, label: {
                        CustomLabel(title: "Try now", image:Image(systemName: "square.and.arrow.up"), foregroundColor: .green, font: Font.system(size: 16.0))
                    })
                    Spacer()
                    Button(action: {
                        
                    }, label: {
                        CustomLabel(title: "Details", image:Image(systemName: "pencil"), foregroundColor: .blue, font: Font.system(size: 16.0))
                    })
                    Spacer()
                    Button(action: {
                        
                    }, label: {
                        CustomLabel(title: "Configuration", image:Image(systemName: "gearshape"), foregroundColor: .gray, font: Font.system(size: 14.0))
                    })
                }
            }
        }
        .frame(maxWidth: .infinity)
        .padding()
        .background(
            RoundedRectangle(cornerRadius: 15)
                .fill(Color.white)
                .shadow(color: .gray, radius: 2, x: 0, y: 2)
        )
    }
}

struct UseCasesRow_Previews: PreviewProvider {
    static var previews: some View {
        UseCasesRow(title: "")
    }
}
