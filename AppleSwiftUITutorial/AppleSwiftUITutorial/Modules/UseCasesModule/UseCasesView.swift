//
//  UseCasesView.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 08/04/22.
//

import SwiftUI
import Combine

public struct ABC {
    var name: String
}

struct UseCasesView: View {
   
    @StateObject private var viewModel: ViewModel
    
    public init(viewModel: ViewModel = .init()) {
        _viewModel = StateObject(wrappedValue: viewModel)
    }
    
    var body: some View {
        NavigationView {
            List {
                ForEach(viewModel.filteredUseCases ) { item in
                    Section(header: Text("\(item.displayTitle)")) {
                        ForEach(item.items, id: \.self) { caseItem in
                            UseCasesRow(title: caseItem)
                                .frame(maxWidth:.infinity)
                                .listRowBackground(Color.clear)
                                .listRowSeparator(.hidden)
                        }
                    }
                }
            }
            .listStyle(.plain)
            .navigationTitle(Text("UseCases"))
            .searchable(text: $viewModel.searchText, placement: .navigationBarDrawer(displayMode: .always), prompt: "Search")
            .navigationBarTitleDisplayMode(.large)
            .onAppear(perform: viewModel.getUseCases)
            .navigationBarItems(trailing:
                                    Button(action: upload) {
                Text("Upload")
            })
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
    
    private func upload() {
        
    }
}

struct UseCasesView_Previews: PreviewProvider {
    static var previews: some View {
        UseCasesView()
    }
}

extension UseCasesView {
    
    class ViewModel: ObservableObject {
        
        private let service: UseCaseService

        var subscription: Set<AnyCancellable> = []
        @Published  private (set) var useCases = [IVUseCase]()
        @Published  private (set) var filteredUseCases = [IVUseCase]()
        @Published var searchText: String = ""
        
        init(service: UseCaseService = UseCaseInteractor()) {
            self.service = service
            processInput()
        }
        
        private func processInput() {
            
            $searchText
                 .debounce(for: .milliseconds(800), scheduler: RunLoop.main) // debounces the string publisher, such that it delays the process of sending request to remote server.
                 .removeDuplicates()
                 .sink { [unowned self] searchPublisher in
                     if searchPublisher.isEmpty {
                         self.filteredUseCases = self.useCases
                     } else {
                         self.filteredUseCases = self.useCases.filter {
                             $0.category.localizedCaseInsensitiveContains(searchPublisher)
                         }
                     }
                 }.store(in: &subscription)
            
        }
        
        func getUseCases() {
            useCases = service.getUseCases()
        }

    }
    
}

private extension IVUseCase {
    var displayTitle: String {
        "\(category) (\(items.count - 1))"
    }
}
