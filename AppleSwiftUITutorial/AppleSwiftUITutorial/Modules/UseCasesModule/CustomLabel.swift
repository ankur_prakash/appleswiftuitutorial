//
//  CustomLable.swift
//  IVA
//
//  Created by Ankur Prakash on 04/04/22.
//

import SwiftUI

struct CustomLabel: View {
    
    var title: String
    var image: Image
    var foregroundColor: Color = .black
    var font: Font = .subheadline
    
    var body: some View {
        HStack(spacing:4.0) {
            image
            Text(title)
                .foregroundColor(foregroundColor)
                .font(font)
        }
    }
}

struct CustomLabel_Previews: PreviewProvider {
    static var previews: some View {
        CustomLabel(title: "Try Now", image: Image(systemName: "star"))
    }
}
