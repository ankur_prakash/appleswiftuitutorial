//
//  3ViewTest.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 05/04/22.
//

import SwiftUI

struct ThreeViewTest: View {
    
    var body: some View {
        NavigationView {
        GeometryReader { proxy in
            VStack(spacing: 0.0) {
                Text("1")
                    .frame(width: proxy.size.width, height: proxy.size.height * 0.25)
                .background(Color.red)
                Text("2")
                .frame(width: proxy.size.width, height: proxy.size.height * 0.5)
                .background(Color.yellow)
                Text("3")
                .frame(width: proxy.size.width, height: proxy.size.height * 0.25)
                .background(Color.green)
            }
            .frame(maxWidth:.infinity)
        }
        .navigationTitle(Text("Hello"))
        .navigationBarTitleDisplayMode(.inline)
    }
    }
}

struct ThreeViewTest_Previews: PreviewProvider {
    static var previews: some View {
        ThreeViewTest()
    }
}

