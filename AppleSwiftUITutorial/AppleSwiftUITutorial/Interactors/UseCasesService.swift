//
//  UseCasesService.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 08/04/22.
//

import Foundation

public protocol UseCaseService {
    
    func getUseCases() -> [IVUseCase]
}
