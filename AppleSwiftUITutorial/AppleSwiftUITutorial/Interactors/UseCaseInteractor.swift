//
//  UseCaseInteractor.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 08/04/22.
//

import Foundation

public class UseCaseInteractor {
    
}

extension UseCaseInteractor: UseCaseService {
    
    public func getUseCases() -> [IVUseCase] {
      
        guard let fileUrl = Bundle.main.url(forResource: "UseCases", withExtension: "json"), let jsonData = try? Data(contentsOf: fileUrl), let list = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [[String: Any]] else {
            return []
        }
        print("list = \(list)")
        return list.compactMap {
            guard let category = $0["category"] as? String,
                  let items = $0["usecases"] as? [String] else { return nil }
            return IVUseCase(category: category, items: items)
        }
    }
}
