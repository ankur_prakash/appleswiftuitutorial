//
//  PanZoomImageView.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 11/04/22.
//

import UIKit

class PanZoomImageView: UIScrollView {

    @IBOutlet private weak var imageView: UIImageView!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    func commonInit() {
        self.delegate = self
    }
}

extension PanZoomImageView: UIScrollViewDelegate {

//    func scrollViewDidZoom(_ scrollView: UIScrollView) {
//
//    }
//
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        return imageView
//    }
}
