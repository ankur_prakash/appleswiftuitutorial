//
//  ImageZoomManager.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 11/04/22.
//

import Foundation
import SwiftUI

public struct ImageZoomManager: UIViewControllerRepresentable {
    
    public func makeUIViewController(context: Context) -> some UIViewController {
        return ImageZoomViewController.smartInit()
    }
    
    public func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
    public func makeCoordinator() -> Coordinator {
        Coordinator()
    }
    
    public class Coordinator: ObservableObject {
        
    }
}



