//
//  ImageZoomViewController.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 11/04/22.
//

import UIKit

class ImageZoomViewController: UIViewController {

    class func smartInit() -> ImageZoomViewController {
        let sb = UIStoryboard(name: "ImageZoomViewController", bundle: nil)
        return sb.instantiateViewController(withIdentifier: "ImageZoomViewController") as! ImageZoomViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
