//
//  ColoredNavigationBar.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 08/04/22.
//

import Foundation
import SwiftUI

//Fatal error: view modifiers must be value types: ColoredNavigationBar
public struct ColoredNavigationBar: ViewModifier {
    
    var backgroundColor: UIColor
    
    public init(backgroundColor: UIColor) {
       
        self.backgroundColor = backgroundColor
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.backgroundColor = backgroundColor
      
        UINavigationBar.appearance().compactAppearance = navBarAppearance
        UINavigationBar.appearance().standardAppearance = navBarAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance
    }
    
    public func body(content: Content) -> some View {
        content
    }
}
