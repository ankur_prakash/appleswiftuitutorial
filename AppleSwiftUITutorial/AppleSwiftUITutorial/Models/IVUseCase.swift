//
//  IVUseCase.swift
//  AppleSwiftUITutorial
//
//  Created by Ankur Prakash on 08/04/22.
//

import Foundation

public struct IVUseCase: Hashable, Equatable {

    public var category: String
    public var items: [String]
    
    public init(category: String, items: [String]) {
        self.category = category
        self.items = items
    }
    
}

extension IVUseCase: Identifiable {
    public var id: String {
        return category
    }
}
